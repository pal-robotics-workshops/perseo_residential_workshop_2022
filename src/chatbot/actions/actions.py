from typing import Any, Text, Dict, List

from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher


class ActionGoTo(Action):
    def name(self) -> Text:
        return "action_go_to"

    def run(
        self,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any],
    ) -> List[Dict[Text, Any]]:

        location = tracker.get_slot("location")
        dispatcher.utter_message(text="I'm going to %s now!" % location)

        # write here the code to interact with the robot
        print("Going to %s" % location)

        return []
