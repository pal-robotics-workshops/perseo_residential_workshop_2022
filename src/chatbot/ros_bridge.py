import rospy
from hri import HRIListener
from hri_msgs.msg import LiveSpeech
from std_msgs.msg import String

import requests
from flask import Flask, request, jsonify

app = Flask(__name__)

sender = "user"  # to be replaced with /humans/voices/<person id>/speech when possible

rasa_endpoint = "http://localhost:5005/webhooks/rest/webhook"

speech_subscribers = {}

speech_pub = rospy.Publisher("/say", String, queue_size=1)


@app.route("/webhook", methods=["POST"])
def rasa_action():
    data = request.json

    destination = data["tracker"]["slots"]["location"]

    rospy.logwarn("Got destination: %s" % destination)

    res = {"events": [], "responses": [{"text": "Going now to the %s" % destination}]}
    return jsonify(res)


def subscribe_to_speech(voice):

    speech_subscribers[voice.id] = rospy.Subscriber(
        voice.ns + "/speech", LiveSpeech, send_to_rasa
    )


def send_to_rasa(msg):

    if not msg.final:
        return

    text = msg.final
    rospy.loginfo('Heard message: "%s"  -- sending it to Rasa...' % text)

    results = requests.post(
        rasa_endpoint, json={"sender": sender, "message": text}
    ).json()

    for r in results:
        msg = String()
        msg.data = r["text"]
        speech_pub.publish(msg)


if __name__ == "__main__":

    rospy.init_node("rasa_bridge")

    hri = HRIListener()
    hri.on_voice(subscribe_to_speech)

    # use same port as default RASA action server
    app.run(host="0.0.0.0", port=5055)
