#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import rospy
from hri import HRIListener
from hri_msgs.msg import EngagementLevel
import tf2_ros
from tf import transformations

import math

# field of 'attention' of a person
FOV = 60.0 * math.pi / 180

# threshold of 'visual social engagement' to consider engagement. See
# Person.assess_engagement for detailed explanation.
ENGAGEMENT_THR = 0.5

REFERENCE_FRAME = "sellion_link"

listener = None
hri = None

engaged_persons = dict()


class Person:
    def __init__(self, person):

        self.person = person

        # publisher for the engagement status of the person
        self.engagement_status_pub = rospy.Publisher(
            self.person.ns + "/engagement_status",
            EngagementLevel,
            queue_size=1,
        )
        self.level = EngagementLevel.UNKNOWN

    def update_engagement(self):
        """
        Computes the current 'visual social engagement' metric as defined in
        "Measuring Visual Social Engagement from Proxemics and Gaze" (by Webb
        and Lemaignan).
        """

        if not self.person.face:
            rospy.logwarn("No face detected, can not compute engagement")
            return

        #########################################################################
        # computation of Visual Social Engagement, following "Measuring Visual
        # Social Engagement from Proxemics and Gaze" by Webb et Lemaignan

        # compute the person's position 'viewed' from the robot's 'gaze'
        person_from_robot = self.person.face.gaze_transform(from_frame=REFERENCE_FRAME)
        if person_from_robot == tf2_ros.TransformStamped():
            rospy.logwarn(
                "null transform published for person's face %s" % self.person.face.id
            )
            return

        # compute the inverse: the robot 'viewed' from the person's gaze
        trans = [
            person_from_robot.transform.translation.x,
            person_from_robot.transform.translation.y,
            person_from_robot.transform.translation.z,
        ]

        rot = [
            person_from_robot.transform.rotation.x,
            person_from_robot.transform.rotation.y,
            person_from_robot.transform.rotation.z,
            person_from_robot.transform.rotation.w,
        ]

        transform = transformations.concatenate_matrices(
            transformations.translation_matrix(trans),
            transformations.quaternion_matrix(rot),
        )

        robot_from_person = transformations.inverse_matrix(transform)

        # next, compute the measure of visual social engagement

        # [in the following, A denotes the person and B denotes the robot]

        tx, ty, tz = transformations.translation_from_matrix(robot_from_person)
        d_AB = math.sqrt(tx ** 2 + ty ** 2 + tz ** 2)

        ## gaze_AB
        # gaze_AB measures how 'close' B is from the optical axis of A

        # frame conventions:
        #  - the *gaze* is using the 'optical frame' convention: +Z forward, +Y down
        #  - the *reference frame* (normally, the sellion link) is using the +Z up, +X forward orientation
        #
        # -> change coordinates to match those used in
        # paper above
        xB = tz
        yB = tx
        zB = ty

        gaze_AB = 0.0
        if xB > 0:
            gaze_AB = max(0, 1 - (math.sqrt(yB ** 2 + zB ** 2) / (math.tan(FOV) * xB)))

        # gaze_BA measures how 'close' A is from the optical axis of B
        tBA = person_from_robot.transform.translation
        xA = tBA.x
        yA = tBA.y
        zA = tBA.z

        gaze_BA = 0.0
        if xA > 0:
            gaze_BA = max(0, 1 - (math.sqrt(yA ** 2 + zA ** 2) / (math.tan(FOV) * xA)))

        M_AB = gaze_AB * gaze_BA
        rospy.logdebug("gazeAB: %s, gazeBA=%s,  M_AB: %s" % (gaze_AB, gaze_BA, M_AB))

        S_AB = min(1, M_AB / d_AB)

        rospy.logdebug("S_AB: %s" % S_AB)

        if S_AB < ENGAGEMENT_THR:
            self.level = EngagementLevel.DISENGAGED
        else:
            self.level = EngagementLevel.ENGAGED

    def publish_engagement(self):
        engagement_msg = EngagementLevel()
        engagement_msg.level = self.level
        self.engagement_status_pub.publish(engagement_msg)

    def close(self):
        self.level = EngagementLevel.UNKNOWN
        self.publish_engagement()
        self.engagement_status_pub.unregister()


def get_tracked_humans():

    # get the list of IDs of the currently visible persons
    persons = hri.tracked_persons.keys()

    # anyone who disappeared?
    for id in list(engaged_persons.keys()):
        if id not in persons:
            engaged_persons[id].close()
            del engaged_persons[id]

    # any new person?
    for id, person in hri.tracked_persons.items():
        if id not in engaged_persons:
            engaged_persons[id] = Person(person)

        engaged_persons[id].update_engagement()
        engaged_persons[id].publish_engagement()


if __name__ == "__main__":

    nh = rospy.init_node("engagement_detector")

    buffer = tf2_ros.Buffer()
    listener = tf2_ros.TransformListener(buffer)

    hri = HRIListener()

    rate = rospy.Rate(10)

    while not rospy.is_shutdown():
        get_tracked_humans()
        rate.sleep()
