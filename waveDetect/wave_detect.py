"""
1. This script will subscribe to /humans/bodies/tracked to extract "ids" 
2. Subscribe to  /humans/bodies/"ids"/skeleton to extract Skeleton2D.RIGHT_SHOULDER, Skeleton2D.RIGHT_ELBOW,Skeleton2D.RIGHT_WRIST 
3. Calulate the distance between the right shoulder and right elbow and right wrist
4. If the distances match a thershold then publish to /humans/bodies/"ids"/skeleton/wave True or False. 
"""
import rospy
import cv2
import numpy as np
import csv
import sys
import os
import rosbag
import cv_bridge
import matplotlib.pyplot as plt
from cv_bridge import CvBridge
import pyinputplus as pyip
import time
import std_msgs.msg
from hri_msgs.msg import Skeleton2D
from hri_msgs.msg import IdsList


Right_shoulder = np.zeros(2)
Right_elbow = np.zeros(2)
Right_wrist = np.zeros(2)


class Wave_detect():
    def __init__(self):

        self.Right_shoulder = np.zeros(2)
        self.Right_elbow = np.zeros(2)
        self.Right_wrist = np.zeros(2)
        self.ids = 0
        self.distance = 0
        self.distance2 = 0
        self.distance3 = 0


        

    def callback_ids(self,data):
        for i in range(len(data.ids)):
            self.ids = data.ids[i]
            # print('ID:',self.ids)
            # self.main()


    def callback_skeleton(self,data):
        self.Right_shoulder[0] = data.skeleton[2].x
        self.Right_shoulder[1] = data.skeleton[2].y
        self.Right_elbow[0] = data.skeleton[3].x
        self.Right_elbow[1] = data.skeleton[3].y
        self.Right_wrist[0] = data.skeleton[4].x
        self.Right_wrist[1] = data.skeleton[4].y

        self.Left_shoulder[0] = data.skeleton[5].x
        self.Left_shoulder[1] = data.skeleton[5].y
        self.Left_elbow[0] = data.skeleton[6].x
        self.Left_elbow[1] = data.skeleton[6].y
        self.Left_wrist[0] = data.skeleton[7].x
        self.Left_wrist[1] = data.skeleton[7].y

        self.distance = np.sqrt((self.Right_shoulder[0] - self.Right_elbow[0])**2 + (self.Right_shoulder[1] - self.Right_elbow[1])**2) # 0.296
        self.distance2 = np.sqrt((self.Right_elbow[0] - self.Right_wrist[0])**2 + (self.Right_elbow[1] - self.Right_wrist[1])**2) # 0.81
        self.distance3 = np.sqrt((self.Right_shoulder[0] - self.Right_wrist[0])**2 + (self.Right_shoulder[1] - self.Right_wrist[1])**2) # 0.571
        # print(self.distance,self.distance2,self.distance3)

        # self.main()





    def publish_wave(self):
        # Publish to /humans/bodies/"ids"/skeleton/wave a binary message indicating whether the user is waving using distance, distance2, distance3
        pub = rospy.Publisher("/humans/bodies/" + str(self.ids) + "/skeleton/wave",
                            std_msgs.msg.Bool,
                            queue_size=10)
        if self.distance > 0.1 and self.distance < 0.25 and self.distance2 > 0.1 and self.distance2 < 0.25 and self.distance3 > 0.15 and self.distance3 < 0.25:
            pub.publish(True)
        else:
            pub.publish(False)
        # self.main()

    def main(self):
        rospy.init_node('wave_detect', anonymous=True)
    
        while not rospy.is_shutdown():

            rospy.Subscriber("/humans/bodies/tracked",
                    IdsList,
                    self.callback_ids)

            if self.ids != None:
                rospy.Subscriber("/humans/bodies/" + str(self.ids) + "/skeleton",
                Skeleton2D,
                self.callback_skeleton)
                self.publish_wave()
                print('ID:',self.ids, 'Distance:',self.distance, 'Distance2:',self.distance2, 'Distance3:',self.distance3)
            else:
                pass
            time.sleep(0.1)

if __name__=='__main__':
    WD = Wave_detect()
    WD.main()







                